package com.example.recuperacioncorte2;

public class IMC {
  private int id;
  private float altura;
  private float peso;
  private float imc;

  public IMC() {

  }

  public IMC(int id, float altura, float peso, float imc) {
    this.id = id;
    this.altura = altura;
    this.peso = peso;
    this.imc = imc;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public float getAltura() {
    return altura;
  }

  public void setAltura(float altura) {
    this.altura = altura;
  }

  public float getPeso() {
    return peso;
  }

  public void setPeso(float peso) {
    this.peso = peso;
  }

  public float getImc() {
    return imc;
  }

  public void setImc(float imc) {
    this.imc = imc;
  }

}
