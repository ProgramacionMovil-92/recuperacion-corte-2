package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.recuperacioncorte2.IMC;

import java.util.ArrayList;

public class ImcDb implements Persistencia, Proyeccion{

  private Context context;
  private ImcDbHelper helper;
  private SQLiteDatabase db;

  public ImcDb(Context context, ImcDbHelper helper){
    this.context=context;
    this.helper=helper;
  }

  public ImcDb(Context context){
    this.context=context;
    this.helper=new ImcDbHelper(this.context);
  }

  @Override
  public void openDataBase() {
    db=helper.getWritableDatabase();
  }

  @Override
  public void closeDataBase() {
    helper.close();
  }

  public long insertImc(IMC imc) {

    ContentValues values=new ContentValues();

    values.put(DefineTabla.IMC.COLUMN_NAME_ALTURA, imc.getAltura());
    values.put(DefineTabla.IMC.COLUMN_NAME_PESO, imc.getPeso());
    values.put(DefineTabla.IMC.COLUMN_NAME_IMC, imc.getImc());
    this.openDataBase();
    long num = db.insert(DefineTabla.IMC.TABLE_NAME, null, values);
    this.closeDataBase();
    Log.d("agregar","insertVenta: "+num);

    return num;
  }

  public long updateImc(IMC imc) {
    ContentValues values=new ContentValues();

    values.put(DefineTabla.IMC.COLUMN_NAME_ALTURA, imc.getAltura());
    values.put(DefineTabla.IMC.COLUMN_NAME_PESO, imc.getPeso());
    values.put(DefineTabla.IMC.COLUMN_NAME_IMC, imc.getImc());
    this.openDataBase();
    long num = db.update(
      DefineTabla.IMC.TABLE_NAME,
      values,
      DefineTabla.IMC.COLUMN_NAME_ID+"="+imc.getId(),
      null);
    this.closeDataBase();
    Log.d("agregar","insertVenta: "+num);

    return num;
  }

  @Override
  public void deleteImc(int id) {
    this.openDataBase();
    db.delete(
      DefineTabla.IMC.TABLE_NAME,
      DefineTabla.IMC.COLUMN_NAME_ID+"=?",
      new String[] {String.valueOf(id)});
    this.closeDataBase();
  }

  @Override
  public IMC getImc(String id) {
    db = helper.getWritableDatabase();

    String stringId = String.valueOf(id);

    Cursor cursor = db.query(
      DefineTabla.IMC.TABLE_NAME,
      DefineTabla.IMC.REGISTRO,
      DefineTabla.IMC.COLUMN_NAME_ID + " = ?",
      new String[]{stringId},
      null, null, null);

    if (cursor.moveToFirst()) {
      IMC imc = readImc(cursor);
      cursor.close();
      return imc;
    } else {
      cursor.close();
      return null; // No se encontró en la base de datos
    }
  }

  @Override
  public ArrayList<IMC> allImc() {
    db=helper.getWritableDatabase();

    Cursor cursor=db.query(
      DefineTabla.IMC.TABLE_NAME,
      DefineTabla.IMC.REGISTRO,
      null, null, null, null, null);
    ArrayList<IMC> imcs = new ArrayList<IMC>();
    cursor.moveToFirst();

    while(!cursor.isAfterLast()){
      IMC imc=readImc(cursor);
      imcs.add(imc);
      cursor.moveToNext();
    }

    cursor.close();
    return imcs;
  }

  @Override
  public IMC readImc(Cursor cursor) {
    IMC imc=new IMC();

    imc.setId(cursor.getInt(0));
    imc.setAltura(cursor.getInt(1));
    imc.setPeso(cursor.getInt(2));
    imc.setImc(cursor.getFloat(3));

    return imc;
  }

}
