package Modelo;

import com.example.recuperacioncorte2.IMC;

public interface Persistencia {

  public void openDataBase();
  public void closeDataBase();
  public long insertVenta(IMC imc);
  public long updateVenta(IMC imc);
  public void deleteVentas(int id);

}
