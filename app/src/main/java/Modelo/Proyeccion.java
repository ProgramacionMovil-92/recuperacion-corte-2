package Modelo;

import android.database.Cursor;

import com.example.recuperacioncorte2.IMC;

import java.util.ArrayList;

public interface Proyeccion {

  public IMC getImc(String id);
  public ArrayList<IMC> allImc();
  public IMC readImc(Cursor cursor);

}
