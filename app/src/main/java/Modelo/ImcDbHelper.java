package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ImcDbHelper extends SQLiteOpenHelper {
  private static final String TEXT_TYPE = " TEXT";
  private static final String INTEGER_TYPE=" INTEGER";
  private static final String COMMA_SEP=" ,";

  private static final String SQL_CREATE_VENTA = "CREATE TABLE " +
    DefineTabla.IMC.TABLE_NAME + " ("+
    DefineTabla.IMC.COLUMN_NAME_ID + " INTEGER PRIMARY KEY, "+
    DefineTabla.IMC.COLUMN_NAME_ALTURA + INTEGER_TYPE + COMMA_SEP +
    DefineTabla.IMC.COLUMN_NAME_PESO + INTEGER_TYPE + COMMA_SEP +
    DefineTabla.IMC.COLUMN_NAME_IMC + INTEGER_TYPE + COMMA_SEP + ")";

  private static final String SQL_DELETE_VENTA = "DROP TABLE IF EXISTS " +
    DefineTabla.IMC.TABLE_NAME;

  private static final String DATABASE_NAME="imc.db";
  private static final int DATABASE_VERSION=1;

  public ImcDbHelper(Context context){
    super(context, DATABASE_NAME, null, DATABASE_VERSION );
  }

  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL(SQL_CREATE_VENTA);
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    sqLiteDatabase.execSQL(SQL_DELETE_VENTA);
    onCreate(sqLiteDatabase);
  }
}
